package geometria;

public interface Geometria {
	// clase abstracta que no puede tener objetos
	public double perimetro();

	public double area();
}